=====================
# Binary Data Types #
=====================

## Signed Byte ##
**URI**: http://www.opengis.net/def/dataType/OGC/0/signedByte
**Description**: 8-bits signed binary integer
**Range**: −128 to +127

## Unsigned Byte ##
**URI**: http://www.opengis.net/def/dataType/OGC/0/unsignedByte
**Description**: 8-bits unsigned binary integer.
**Range**: 

## Unsigned Short ##
**URI**: http://www.opengis.net/def/dataType/OGC/0/unsignedShort
**Description**: 16-bits unsigned binary integer
**Range**: 0 to +65,535

## Signed Short ##
**URI**: http://www.opengis.net/def/dataType/OGC/0/signedShort 
**Description**: 16-bits signed binary integer.
**Range**: −32,768 to +32,767

## Unsigned Short ##
**URI**: http://www.opengis.net/def/dataType/OGC/0/unsignedShort
**Description**: 16-bits unsigned binary integer.
**Range**: 0 to +65,535

## Signed Int ##
**URI**: http://www.opengis.net/ def/dataType/OGC/0/signedInt
**Description**: 32-bits signed binary integer.
**Range**: −2,147,483,648 to +2,147,483,647

## Unsigned Int ##
**URI**: http://www.opengis.net/def/dataType/OGC/0/unsignedInt
**Description**: 32-bits unsigned binary integer.
**Range**: 0 to +4,294,967,295

## Signed Long ##
**URI**: http://www.opengis.net/def/dataType/OGC/0/signedLong
**Description**: 64-bits signed binary integer.
**Range**: −2^63 to +2^63 - 1

## Unsigned Long ##
**URI**: http://www.opengis.net/def/dataType/OGC/0/unsignedLong
**Description**: 64-bits unsigned binary integer.
**Range**: 0 to +2^64 - 1

## Half Precision Float ##
**URI**: http://www.opengis.net/ def/dataType/OGC/0/float16
**Description**: 16-bits single precision floating point number as defined in IEEE 754.
**Range**: 

## Float ##
**URI**: http://www.opengis.net/def/dataType/OGC/0/float32
**Description**: 32-bits single precision floating point number as defined in IEEE 754.
**Range**: 

## Double ##
**URI**: http://www.opengis.net/def/dataType/OGC/0/double or http://www.opengis.net/def/dataType/OGC/0/float64
**Description**: 64-bits double precision floating number as defined in IEEE 754.
**Range**: 

## Long Double ##
**URI**: http://www.opengis.net/ def/dataType/OGC/0/float128
**Description**: 128-bits quadruple precision floating point number as defined in IEEE 754.
**Range**: 

## UTF-8 String (Variable Length) ##
**URI**: http://www.opengis.net/def/dataType/OGC/0/string-utf-8 “byteLength” attribute is not set.
**Description**: Variable length string composed of a 2-bytes unsigned short value indicating its length followed by a sequence of UTF-8 encoded characters as specified by the Unicode Standard (2.5).
**Range**: 

## UTF-8 String* (Fixed Length) ##
**URI**: http://www.opengis.net/def/dataType/OGC/0/string-utf-8 “byteLength” attribute is set.
**Description**: Fixed length string composed of a sequence of UTF-8 encoded characters as specified by the Unicode Standard (2.5), and padded with 0 characters.
**Range**: 


