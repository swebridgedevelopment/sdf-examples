<?xml version="1.0" encoding="UTF-8"?>
<!-- ==================================================================================== 
     This document models a simple binary command. The sensor expects the command:
     "data?". This command is encoded in binary
     
     The sensor responds with a 32-bit integer, a 32-bit float and a 32-bit unsigned int
        
     "XX XX XX XX YY YY YY YY ZZ ZZ ZZ ZZ"
      ___________ ___________ ___________
         int 32     float 32    unint 32
       
               	
      =================================================================================== -->
<sml:PhysicalSystem	xmlns:sml="http://www.opengis.net/sensorml/2.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:swe="http://www.opengis.net/swe/2.0"
	xmlns:xlink="http://www.w3.org/1999/xlink"
	xsi:schemaLocation="http://www.opengis.net/sensorml/2.0 http://schemas.opengis.net/sensorML/2.0/sensorML.xsd" 
	xmlns:gml="http://www.opengis.net/gml/3.2" 
	gml:id="SimpleBinarySensor">
	
	<sml:components>
		<sml:ComponentList>
			<sml:component name="SimpleBinaryCommand">
				<sml:SimpleProcess gml:id="SimpleBinaryCommand">
					<gml:description>Description of a simple binary command with its associated response</gml:description>
					<sml:inputs>
						<sml:InputList>	
							<!-- The input corresponds to the command that will be sent to the sensor -->							
							<sml:input name="command">		
								<sml:DataInterface>
									<sml:data>
										<swe:DataStream>
											<!-- The element type defines the data model -->
											<!-- According to the SWE common data model it is not possible to define
								                 any values inside the elementType, so the values have to be 
								                 specified afterwards -->
											<swe:elementType name="dataModel">
												<swe:DataRecord>
													<swe:field name="textCommand">
														<swe:Text/>															
													</swe:field>
													<!-- NOTE: A block separator is always added at the end of the DataRecord -->													 
												</swe:DataRecord>
											</swe:elementType>
											<!-- Encoding of the command data model -->
											<swe:encoding>													
												<swe:BinaryEncoding 
													byteOrder="littleEndian" 
													byteEncoding="base64" 
													byteLength="5">
													<!-- byteOrder can be "raw" littleEndian "bigEndian" -->
													<!-- byteEncoding can be "raw" or "Base64" -->
													<!-- note that in order to embed binary data into a XML document, the Base64 encoding
														 has to be used -->												
													<!-- byteLength (optional) can be used to specify the length -->

													<swe:member>
														<!-- Component is used to specify binary encoding for "scalar fields" (Count, Quantity,
															etc.) -->
										 																												
														<swe:Component 
															dataType="http://www.opengis.net/def/dataType/OGC/0/string-utf-8"
															ref="command/dataModel/textCommand"
															byteLength="5"/>
														<!-- Data type should be one of the links provided at the end of this document -->
														<!-- ref refers to the component in the data model -->
														<!-- byteLength (optional can be set to indicate the length of the component -->
														<!-- the reference path has to be compliant with SensorML path rules (see Paths.xml
														     or SensorML standard pag 129)-->

													</swe:member>
												</swe:BinaryEncoding>
											</swe:encoding>							
											<!-- In order to embed binary data into an XML file, it has to be converted into base64 data -->
											<!-- "data?" ==> hex ==> '64 61 74 61 3f' ==> base64 -> 'ZGF0YT8' -->
											<!-- Although base64 is used to encode binary data, the SWE Bridge will always convert this
												 data into "raw" binary data befoer being sent. There are some online tools that can help
							    				 to convert binary data to base64 (i.e. https://cryptii.com/base64-to-binary)  -->
											<swe:values>ZGF0YT8=</swe:values>
										</swe:DataStream>
									</sml:data>
								</sml:DataInterface>
							</sml:input>
						</sml:InputList>
					</sml:inputs>
					<sml:outputs>
						<sml:OutputList>
							<sml:output name="response">								
								<sml:DataInterface>
									<sml:data>										
										<swe:DataStream>
											<swe:elementType name="dataModel">												
													<swe:DataRecord>
														<!-- Defining the variable X (int 32) -->
														<swe:field name="Xint">
															<swe:Count definition="http://link_to_ontology_here/Xint"/>											
														</swe:field>											
														<!-- Defining the variable Y (float 32) -->														
														<swe:field name="Yfloat">
															<swe:Quantity definition="http://link_to_ontology_here/Xint">
																<swe:uom code="someUnits"/>
															</swe:Quantity>
														</swe:field>
														<!-- Defining the variable Z (unsigned int 32) -->
														<swe:field name="Zuint">
															<swe:Count definition="http://link_to_ontology_here/Zuint"/>
														</swe:field>																				
													</swe:DataRecord>
																								
											</swe:elementType>
											<swe:encoding>												
												<swe:BinaryEncoding byteOrder="littleEndian" byteEncoding="raw" byteLength="96">
													<!-- As the data structure has a fixed size, the byteLength variable is added -->
													
													<!-- Defining encoding for X (int 32)-->
													<swe:member>																
														<swe:Component 
															dataType="http://www.opengis.net/ def/dataType/OGC/0/signedInt"
															ref="outputs/response/dataModel/Xint"/>
													</swe:member>
													<!-- Defining encoding for Y (float 32)-->
													<swe:member>																
														<swe:Component 
															dataType="http://www.opengis.net/def/dataType/OGC/0/float32"
															ref="outputs/response/dataModel/Yfloat"/>
													</swe:member>
													<!-- Defining encoding for Z (unsigned int 32)-->
													<swe:member>																
														<swe:Component 
															dataType="http://www.opengis.net/def/dataType/OGC/0/unsignedInt"
															ref="outputs/response/dataModel/Zuint"/>
													</swe:member>
												</swe:BinaryEncoding>
											</swe:encoding>
											<!-- values are not encoded as they depend on the sensor response -->
											<swe:values/>
										</swe:DataStream>
									</sml:data>
								</sml:DataInterface>
							</sml:output>
						</sml:OutputList>
					</sml:outputs>
				</sml:SimpleProcess>			
			</sml:component>
		</sml:ComponentList>
	</sml:components>
	
	
</sml:PhysicalSystem>


<!-- List of Data types: 
				
		== Signed Byte ==
		URI: http://www.opengis.net/def/dataType/OGC/0/signedByte
		Description: 8-bits signed binary integer
		Range: −128 to +127
		
		== Unsigned Byte ==
		URI: http://www.opengis.net/def/dataType/OGC/0/unsignedByte
		Description: 8-bits unsigned binary integer.
		Range: 
		
		== Unsigned Short ==
		URI: http://www.opengis.net/def/dataType/OGC/0/unsignedShort
		Description: 16-bits unsigned binary integer
		Range: 0 to +65,535
		
		== Signed Short ==
		URI: http://www.opengis.net/def/dataType/OGC/0/signedShort 
		Description: 16-bits signed binary integer.
		Range: −32,768 to +32,767
		
		== Unsigned Short ==
		URI: http://www.opengis.net/def/dataType/OGC/0/unsignedShort
		Description: 16-bits unsigned binary integer.
		Range: 0 to +65,535
		
		== Signed Int ==
		URI: http://www.opengis.net/ def/dataType/OGC/0/signedInt
		Description: 32-bits signed binary integer.
		Range: −2,147,483,648 to +2,147,483,647
		
		== Unsigned Int ==
		URI: http://www.opengis.net/def/dataType/OGC/0/unsignedInt
		Description: 32-bits unsigned binary integer.
		Range: 0 to +4,294,967,295
		
		== Signed Long ==
		URI: http://www.opengis.net/def/dataType/OGC/0/signedLong
		Description: 64-bits signed binary integer.
		Range: −2^63 to +2^63 - 1
		
		== Unsigned Long ==
		URI: http://www.opengis.net/def/dataType/OGC/0/unsignedLong
		Description: 64-bits unsigned binary integer.
		Range: 0 to +2^64 - 1
		
		== Half Precision Float ==
		URI: http://www.opengis.net/ def/dataType/OGC/0/float16
		Description: 16-bits single precision floating point number as defined in IEEE 754.
		Range: 
		
		== Float ==
		URI: http://www.opengis.net/def/dataType/OGC/0/float32
		Description: 32-bits single precision floating point number as defined in IEEE 754.
		Range: 
		
		== Double ==
		URI: http://www.opengis.net/def/dataType/OGC/0/double or http://www.opengis.net/def/dataType/OGC/0/float64
		Description: 64-bits double precision floating number as defined in IEEE 754.
		Range: 
		
		== Long Double ==
		URI: http://www.opengis.net/ def/dataType/OGC/0/float128
		Description: 128-bits quadruple precision floating point number as defined in IEEE 754.
		Range: 
		
		== UTF-8 String (Variable Length) ==
		URI: http://www.opengis.net/def/dataType/OGC/0/string-utf-8 “byteLength” attribute is not set.
		Description: Variable length string composed of a 2-bytes unsigned short value indicating its length followed by a sequence of UTF-8 encoded characters as specified by the Unicode Standard (2.5).
		Range: 
		
		== UTF-8 String* (Fixed Length) ==
		URI: http://www.opengis.net/def/dataType/OGC/0/string-utf-8 “byteLength” attribute is set.
		Description: Fixed length string composed of a sequence of UTF-8 encoded characters as specified by the Unicode Standard (2.5), and padded with 0 characters.
		Range: 	
	-->