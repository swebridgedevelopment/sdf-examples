# README #

This repository contains a set of example Sensor Deployment Files (SDF). A SDF if a SensorML document used to model an instrument and its associated acquisition proceudres. An SDF can be used with an interpreter software, i.e. the SWE Bridge, to automatically configure an acquisition process based on a sensor description.

In this repository SDF snippets encoding different sensor outputs as well as complete SDF examples can be found. 

**author**: Enoc Mart�nez, Daniel M. Toma         
**contact**: enoc.martinez@upc.edu, daniel.mihai.toma@upc.edu                           
**organization**: Universitat Polit�cnica de Catalunya (UPC) 